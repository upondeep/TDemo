import { NgModule, Optional, SkipSelf, ModuleWithProviders } from '@angular/core';
import { GlobalService } from '../services/global.service';
import { GlobalEventService } from '../services/global.event.service';

export const providers = [
  GlobalService,
  GlobalEventService,
];

@NgModule({
  imports: [
  ],
  declarations: []
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error("CoreModule loaded, only import in AppModule");
    }
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [...providers],
    }
  }
}
