import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// import { InfoComponent } from '../home/info/info.component';

const routes: Routes = [
  //primary route
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: '**', redirectTo: '/home', pathMatch: 'full' },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { enableTracing: true }),
  ],
  declarations: [],
  exports: [RouterModule]
})
export class RoutingModule { }
